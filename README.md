Notes:

for gitlab, don't forget to create an authentication token and run the `ansible-playbook` command
with the additional `extra-vars` flag

```bash
ansible-playbook -i ./ansible/inventory.yml ansible/playbook.yml --private-key wintermute.pem --extra-vars "gitlab_token=your-token"
```

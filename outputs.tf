output "public_ip" {
  value = aws_instance.wintermute_instance.public_ip
}

output "eip" {
  value = aws_eip.wintermute_elastic_ip.public_ip
}


variable "wintermute_region" {
  type = string
  default = "eu-west-3" # paris
}

variable "wintermute_default_az" {
  type = string
  default = "eu-west-3a"
}

variable "wintermute_keypair_name" {
  type = string
  default = "wintermute"
}

variable "wintermute_instance_type" {
  type = string
  default = "t3a.large"
}

variable "wintermute_ami" {
  type = string
  description = "debian 12 in eu-west-3 (update this with the AMI of your desired region)"
  default = "ami-087da76081e7685da" 
}

variable "wintermute_instance_role_policies" {
  type = list(string)
  description = "name of the AWS managed policies to apply to our EC2 instance"
  default = ["arn:aws:iam::aws:policy/PowerUserAccess"]
}
